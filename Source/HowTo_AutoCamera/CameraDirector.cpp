// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraDirector.h"
#include "Kismet/GameplayStatics.h"
#include "EngineGlobals.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const float timeBetweenCameraChanges = 2.0f;
	const float smoothBlendTime = 0.75f;
	timeToNextCameraChange -= DeltaTime; 
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Screen Message"));
	if (timeToNextCameraChange <= 0.0f)
	{
		
		timeToNextCameraChange += timeBetweenCameraChanges;

		//FInd the aactor that handles control for the local player.
		APlayerController* ourPlayerController = UGameplayStatics::GetPlayerController(this, 0);
		if (ourPlayerController)
		{
			if ((ourPlayerController->GetViewTarget() != cameraOne) && (cameraOne != nullptr))
			{
				//ourPlayerController->SetViewTarget(cameraOne);
				ourPlayerController->SetViewTargetWithBlend(cameraOne, smoothBlendTime);
			}
			else if ((ourPlayerController->GetViewTarget() != cameraTwo) && (cameraTwo != nullptr))
			{
				ourPlayerController->SetViewTargetWithBlend(cameraTwo, smoothBlendTime);
			}
		}
	}
	
}

